<?php
class Car{
    private $engine;
    public function __construct(Engine $engine){
        $this->engine = $engine;
    }
    public function drive(){
        $this->engine->start();
        if($this->engine instanceof IElEngine){
          $this->engine->check();
        }
    }
}
 
class Engine implements IEngine, IElEngine {
    public function start(){
        echo "--Engine--<br>";
    }
    public function check(){
        echo '--Check--';
    }
}
interface IEngine {
  public function start();
}

interface IElEngine {
  public function check();
}

$car = new Car(new Engine());
$car->drive();