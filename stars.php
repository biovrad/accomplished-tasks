<?php 

$n = 5;
$k = 1 * $n - 1; 
for ($i = 0; $i < $n; $i++) 
{ 
	for ($j = 0; $j < $k; $j++) 
		echo " "; 
	$k = $k - 1; 
	for ($j = 0; $j <= $i; $j++ ) 
	{ 
	echo "* "; 
	} 
	echo "\n"; 
} 
/*

    * 
   * * 
  * * * 
 * * * * 
* * * * * 

*/

$height=4;
for($y=1; $y<=$height; $y++){
	$l = $height-$y;
	$r = $y*2-1;
	echo str_repeat(' ', $l ).str_repeat('*', $r )."\n";
}
/*

   *
  ***
 *****
*******

*/

function a( $c ) {
    static $i = 1;
    return $c < $i ? "" : str_repeat("*", $i++)."\n".a($c);
}

print a(5);


*
**
***
****
*****

+++++++++++++++++++++++
function a( $c ) {
    static $i = 1;
    return $c < $i ? "" : str_repeat(" ", $c-$i).str_repeat("* ", $i++)."\n".a($c);
}
print a(5);

/*
    * 
   * * 
  * * * 
 * * * * 
* * * * * 

*/


$height=4;
for($y=1; $y<=$height; $y++){
    echo str_repeat(' ',$height-$y);
    echo str_repeat('*',$y*2-1)." \n";
}

*/
   * 
  *** 
 ***** 
******* 

*/

for($i=0;$i<=5;$i++){  
    for($k=5;$k>=$i;$k--){  
    	echo " ";  
    }  
    for($j=1;$j<=$i;$j++){  
    	echo "* ";  
    }  
    echo "\n";  
}

for($i=4;$i>=1;$i--){  
    for($k=5;$k>=$i;$k--){  
        echo " ";  
    }  
    for($j=1;$j<=$i;$j++){  
        echo "* ";  
    }  
echo "\n";  
}

/*
     * 
    * * 
   * * * 
  * * * * 
 * * * * * 
  * * * * 
   * * * 
    * * 
     * 
*/

for($y=1; $y<=3; $y++){
    echo str_repeat('*', 4 )."\n";
}

/*

****
****
****

*/
